extern crate bytes;
extern crate failure;
extern crate tokio_core;
extern crate tokio_io;
extern crate tokio_serial;

use bytes::{BufMut, BytesMut};
use std::io;
use tokio_core::reactor::Core;
use tokio_io::codec::{Decoder, Encoder};
use tokio_serial::{Serial, SerialPortSettings};

type Result<T> = std::result::Result<T, failure::Error>;

fn main() {
    // No sophisticated error handling for this example.
    run().unwrap();
}

fn run() -> Result<()> {
    let path = "/dev/ttyUSB0";

    let core = Core::new()?;
    let handle = core.handle();

    let settings = SerialPortSettings::default();
    let port = Serial::from_path(path, &settings, &handle)?;

    // What do I need to do here? I would like to get functionality that
    // can be expressed similar to this pseudo-code, which should
    // be run by the core:
    /*
    let client = Client::new(port)?;
    client.send_message('x')
        .and_then(|response| println!("Received {}", response));
    client.send_message('y')
        .and_then(|response| println!("Received {}", response));
    client.send_message('z')
        .and_then(|response| println!("Received {}", response));

    let timeout: Duration = …;
    // this command should wait for any pending responses, and
    // shut down after either receiving the last response, or
    // reaching the timeout
    client.process_pending_requests(timeout);

    client.shutdown();
     */

    Ok(())
}

// For this example, we just implement an echo protocol where each
// byte is a message and gets mirrored back by the serial port.
// Of course the real-world protocol is not that simple.
pub struct Codec;

impl Decoder for Codec {
    type Item = u8;
    type Error = io::Error;

    fn decode(
        &mut self,
        src: &mut BytesMut,
    ) -> io::Result<Option<Self::Item>> {
        src.iter().next().map_or_else(|| Ok(None), |b| Ok(Some(*b)))
    }
}

impl Encoder for Codec {
    type Item = u8;
    type Error = io::Error;

    fn encode(
        &mut self,
        item: Self::Item,
        dst: &mut BytesMut,
    ) -> io::Result<()> {
        if dst.has_remaining_mut() {
            Ok(dst.put_u8(item))
        } else {
            Err(io::Error::new(io::ErrorKind::WouldBlock, "Buffer full"))
        }
    }
}
